import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { isTemplateElement } from '@babel/types';

  function Square(props) {
    if (props.winnerSquare) {
      return (
        <button
          className="square-win"
          onClick={props.onClick}
        >
          {props.value}
        </button>
      );
    } else {
      return (
        <button
          className="square"
          onClick={props.onClick}
        >
          {props.value}
        </button>
      );
    }
  }

  class Board extends React.Component {
    constructor(props) {
      super(props);
    }

    renderSquare(i) {
      if (i === this.props.winnerSquares[0] || i === this.props.winnerSquares[1] || i === this.props.winnerSquares[2]) {
        return (
          <Square
            winnerSquare={true}
            value={this.props.squares[i]}
            onClick={() => this.props.onClick(i)}
          />);
      }
      return (
      <Square
        winnerSquare={false}
        value={this.props.squares[i]}
        onClick={() => this.props.onClick(i)}
      />);
    }
  
    render() {
      let all = [];
      for (var i = 0; i < 3; i++) {
        let items = [];
        for (var j = 0; j < 3; j++) {
          items.push(this.renderSquare((i * 3) + j));
        }
        all.push(<div className="board-row">{items}</div>);
      }
      return (
        <div>
          {all}
        </div>
      );
    }
  }
  
  class Game extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        history: [{
          squares: Array(9).fill(null),
          place: -1,
        }],
        xIsNext: true,
        stepNumber: 0,
        winnerSquares: Array(3).fill(null),
        reversed: false,
      }
    }

    handleClick(i) {
      const history = this.state.history.slice(0, this.state.stepNumber + 1);
      const current = history[history.length - 1];
      const squares = current.squares.slice();
      if (calculateWinner(squares) || squares[i]) {
          return;
      } else {
          squares[i] = this.state.xIsNext ? 'X' : 'O';
          this.setState({
              history: history.concat([{
                squares: squares,
                place: i,
              }]),
              stepNumber: history.length,
              xIsNext: !this.state.xIsNext,
          });
      }
    }

    sortHistory() {
      this.setState({
        reversed: !this.state.reversed,
      });
    }

    jumpTo(step) {
      this.setState({
        stepNumber: step,
        xIsNext: (step % 2) === 0,
      });
    }

    updateWinner(winner) {
      this.setState({
        winnerSquares: winner,
      });
    }

    render() {
      const history = this.state.history;
      const current = history[this.state.stepNumber];
      const winner = calculateWinner(current.squares);

      let moves = history.map((step, move) => {
        const desc = move ?
          'Go to move #' + move + '(Col: ' + history[move].place % 3 + ', Row: ' + Math.floor(history[move].place / 3) + ')':
          'Go to move start';
        if (history[move].place === current.place) {
          return (
           <li key={move}>
             <button onClick={() => this.jumpTo(move)}><b>{desc}</b></button>
           </li>
          );
        }
        else {
          return (
              <li key={move}>
                 <button onClick={() => this.jumpTo(move)}>{desc}</button>
              </li>
           );
        }
      });

      let status;
      if (winner && this.state.winnerSquares[0] === null) {
        this.updateWinner(winner);
      }
      if (winner) {
        status = 'Winner: ' + current.squares[winner[0]];
      }
      else {
        status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');
      }

      var draw = 0;
      for (var i = 0; i < 9; i++) {
        if (current.squares[i] === null) {
          draw++;
        }
      }
      if (draw === 0) {
        status = "It's a draw !"
      }
      
      if (this.state.reversed) {
        moves = moves.reverse();
      }

      return (
        <div className="game">
          <div className="game-board">
            <Board
              winnerSquares={this.state.winnerSquares} 
              squares={current.squares}
              onClick={(i) => this.handleClick(i)}
            />
          </div>
          <div className="game-info">
            <div>{status}</div>
            <button onClick={() => this.sortHistory()}>Sort</button>
            <ol>{moves}</ol>
          </div>
        </div>
      );
    }
  }

  function calculateWinner(squares) {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
        return [a, b, c];
      }
    }
    return null;
  }
  
  // ========================================
  
  ReactDOM.render(
    <Game />,
    document.getElementById('root')
  );
  